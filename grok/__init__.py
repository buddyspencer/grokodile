from pygrok import Grok

class grok:
    def __init__(self, p):
        self.pattern = p

    def tryGrok(self, l):
        grokked = []
        g = Grok(self.pattern)
        for s in l:
            gr = g.match(s)
            if gr != None:
                grokked.append(gr)
        return grokked

    def tryGrokLine(self, l):
        g = Grok(self.pattern)
        gr = g.match(l)
        if gr != None:
           return gr
        return None