#!/usr/bin/env python3
import datetime
import grok
import yaml
import nsq
import el
import copy
from logbuddy import logbuddy

def handle(message):
    global g
    global tstamp
    global queued

    #grokked = g.tryGrok(str(body))
    queued.append(str(message.body))
    #grokked = g.tryGrokLine(str(message.body))
    message.finish()

    #if grokked != None:
    #    es.send(grokked)

    if tstamp < datetime.datetime.now() - datetime.timedelta(seconds=intervall):
        cq = copy.deepcopy(queued[:len(queued)])
        grokked = g.tryGrok(cq)
        cq = []
        es.add(grokked)
        queued = queued[len(queued):]
        log.Info("Sending {} logs".format(es.getLength()))
        es.bulk()
        #es.send()
        tstamp = datetime.datetime.now()
    return True

log = logbuddy.log()
intervall = 5
tstamp = datetime.datetime.now()
queued = []

with open("config.yml", "r") as ymlfile:
    config = yaml.load(ymlfile)

g = grok.grok(config["grok"])
es = el.ES(config["elasticsearch"]["index"], config["elasticsearch"]["urls"], config["elasticsearch"]["version"])

r = nsq.Reader(message_handler=handle,
               lookupd_http_addresses=['http://{}:4161'.format(config["nsq"]["host"])],
               topic=config["nsq"]["queue"], channel='grokodile', lookupd_poll_interval=15, msg_timeout=90)

nsq.run()