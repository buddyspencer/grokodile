FROM python:3.6.2
COPY . ./
RUN pip install --no-cache-dir -r requirements.txt

CMD [ "python", "-u", "./grokodil.py" ]
