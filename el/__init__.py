import datetime
import time
import certifi
import hashlib
import threading
import copy
from elasticsearch5 import Elasticsearch as Elasticsearch5
from elasticsearch5 import helpers as Helpers5
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from random import randint

class ES:
    logs = []

    def __init__(self, index, urls, version=6):
        self.index = index
        self.elasticurls = urls
        if version == 6:
            self.es = Elasticsearch(self.elasticurls)
            self.helpers = helpers
        elif version == 5:
            self.es = Elasticsearch5(self.elasticurls)
            self.helpers = Helpers5

    def add(self, log):
        if isinstance(log, (list,)):
            for l in log:
                self.logs.append(l)
        else:
            self.logs.append(log)

    def getLength(self):
        return len(self.logs)

    def send(self, log):
        log["@timestamp"] = self.__getTimeStamp(log["timestamp"])
        #t = datetime.datetime.now()
        #log["@timestamp"] = t.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        self.es.index(index=self.index, id=self.__createID("{}-{}".format(str(log), datetime.datetime.now())), doc_type="logs", body=log)

    #def bulk(self):
    #    len_logs = self.getLength()
    #    clist = copy.deepcopy(self.logs[:len_logs])
    #    t = threading.Thread(target=self.__bulk, args=(clist,))
    #    t.start()
    #    self.logs = self.logs[len_logs:]

    def bulk(self):
        actions = []
        while True:
            if self.es.ping():
                break
            else:
                time.sleep(5)

        for i in self.logs:
            tstamp, indexdate = self.__getTimeStamp(i["timestamp"])
            action = {
                "_index": "{}-{}".format(self.index, indexdate),
                "_type": "logs",
                "_id": self.__createID("{}-{}".format(str(i), datetime.datetime.now())),
                "_source": i
            }
            action["_source"]["@timestamp"] = tstamp
            #t = datetime.datetime.now()
            #action["@timestamp"] = t.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
            actions.append(action)
            if len(actions) == 500:
                self.helpers.parallel_bulk(self.es, actions)
                actions = []

        self.helpers.parallel_bulk(self.es, actions)

        #self.logs = self.logs[len_logs:]
        self.logs = []

    def __createID(self, value):
        return hashlib.md5(value.encode("utf-8")).hexdigest()

    def __getTimeStamp(self, tstamp):
        t = datetime.datetime.fromtimestamp(int(time.mktime(datetime.datetime.strptime(tstamp, "%d/%b/%Y:%H:%M:%S %z").timetuple())))
        return t.strftime('%Y-%m-%dT%H:%M:%S.%fZ'), t.strftime('%Y-%m-%d')